<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/persona.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// P
	'persona_description' => 'persona est une méthode d’authentification proposée par Mozilla. Elle repose sur l’emploi de l’email de l’auteur comme clé de connexion (et non pas d’une URL comme pour OpenID). Ce plugin implémente persona dans SPIP : connexion, création de compte, signature des messages de forum ou des pétitions…

A noter : persona est une technologie expérimentale, et le plugin persona pour SPIP est lui aussi expérimental.',
	'persona_slogan' => 'Authentification des visiteurs via persona'
);
