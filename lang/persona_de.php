<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/persona?lang_cible=de
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'affichage' => 'Anzeigen',

	// C
	'cacher_login_classique' => 'Ausblenden ?',
	'configurer_titre' => 'Persona konfigurieren',

	// E
	'explication_cacher_login_classique' => 'Nach dem Abhaken dieses Kästchens wird das klassische Login-Formular auf der Anemdldeseite standardmäßig ausgeblendet. Es wird nur noch die "Persona" Schaltfläche angezeigt. Darunter wird ein Link das erneute Einblenden ermöglichen.',
	'explication_login' => 'Sie können sich mit Ihrer Mozilla Persona ID (<a href="http://fr.wikipedia.org/wiki/Mozilla_Persona" target="_blank">aide</a>) anmelden.',

	// F
	'form_afficher' => 'Klassisches Login-Formular anzeigen',
	'form_masquer' => 'Klassisches Login-Formular ausblenden'
);
