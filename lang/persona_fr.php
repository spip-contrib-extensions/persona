<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/persona.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'affichage' => 'Affichage',

	// C
	'cacher_login_classique' => 'Masquer ?',
	'configurer_titre' => 'Configurer Persona',

	// E
	'explication_cacher_login_classique' => 'En cochant cette case, le formulaire classique de login sera masqué par défaut sur la page de connexion. Seul le bouton "Persona" sera donc visible. Sous ce bouton, un lien permet de démasquer le formulaire.',
	'explication_login' => 'Vous pouvez vous connecter grâce à votre identifiant Mozilla Persona (<a href="http://fr.wikipedia.org/wiki/Mozilla_Persona" target="_blank">aide</a>)',

	// F
	'form_afficher' => 'Afficher le formulaire de login classique',
	'form_masquer' => 'Masquer le formulaire de login classique'
);
